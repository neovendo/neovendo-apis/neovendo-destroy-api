import boto3
from datetime import datetime, timedelta

TAG_TERMINATE = 'terminate-after'


def lambda_handler(event, context):
    regions_to_check = ['eu-west-1']
    for region in regions_to_check:
        print('Region: ', region)

        ec2_resource = boto3.resource('ec2', region_name=region)
        running_filter = {'Name': 'instance-state-name', 'Values': ['running']}
        instances = ec2_resource.instances.filter(Filters=[running_filter])

        for instance in instances:
            if instance.tags is None:
                continue

            if value := [tag['Value'] for tag in instance.tags if tag['Key'] == TAG_TERMINATE]:
                if check_duration(value[0], instance.launch_time, instance):
                    print(f'Terminating instance {instance.id}')
                    instance.terminate()


def check_duration(duration_string, launch_time, instance):
    # Extract duration to wait
    try:
        if duration_string[-1] == 'm':
            minutes = int(duration_string[:-1])
        elif duration_string[-1] == 'h':
            minutes = int(float(duration_string[:-1]) * 60)
        else:
            print(f'Invalid duration of "{duration_string}" for instance {instance.id}')
            return False
    except:
        print(f'Invalid duration of "{duration_string}" for instance {instance.id}')
        return False

    # Check whether required duration has elapsed
    return datetime.now().astimezone() > launch_time + timedelta(minutes=minutes)
